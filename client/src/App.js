import React from 'react'

import PersonalInfo from './components/index/PersonalInfo'
import './App.css'

const App = () => {
  return(
    <div>
      <div className="block"><span>سایت شخصی</span> <span className="font-weight-bold">مریم احمدی نژاد</span></div>
      <PersonalInfo />
      <div className="row justify-content-center d-none">
        <div className="col-12 col-md-9 p-5">
          <img className="w-100 round" src= {process.env.PUBLIC_URL + '/images/relaxing.jpg'} />
        </div>
      </div>    
    </div>
  )
}

export default App