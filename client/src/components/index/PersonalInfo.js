import React from 'react'
import '../../css-components/index/PersonalInfo.css'

const PersonalInfo = () => {
    return(
        <div>
            <p className="major">کارشناسی ارشد مهندسی پزشکی دانشگاه تهران</p>
        </div>
    )
}

export default PersonalInfo